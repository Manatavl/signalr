﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json;
using System.Linq;
using Microsoft.AspNet.SignalR.Hosting;

namespace Otus.Teaching.PromoCodeFactory.WebHost.SignalR
{
    public class CustomersHub : Hub
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        public CustomersHub(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task GetAllAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            await Clients.All.SendAsync("GetAllAsync", JsonConvert.SerializeObject(response));
        }
        public async Task GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            var response = new CustomerResponse(customer);

            await Clients.All.SendAsync("GetCustomerAsync", JsonConvert.SerializeObject(response));
        }
        public async Task CreateCustomerAsync()
        {
            var request = new CreateOrEditCustomerRequest
            {
                FirstName = "Иван",
                LastName = "Иванов",
                Email = "email12@yandex.ru",
                PreferenceIds = new List<Guid> { Guid.NewGuid()}
            };
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);
            await GetCustomerAsync(customer.Id);
        }
        public async Task Send(string message)
        {
            await Clients.All.SendAsync(message);
        }

    }
}